$(function(){

	$('#capture').click(function(){
		captureData();
	})

})

function captureData(){
	$('#loader').show();

	$.getJSON('question/captureData', function(result){
		alert('Dados capturados com sucesso!');
	}).fail(function(){
		alert('Erro ao capturar dados, por favor tente novamente.');
	}).always(function(){
		$('#loader').hide();
	});
}
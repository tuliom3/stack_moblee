<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

	private $SORT_OPTIONS = ["question_id", "title", "owner_name", "score", "creation_date", "link", "is_answered"];

	public function index()
	{
		$data = self::getData();

		$items = self::setItemsStructure($data);

		$items = self::applyParameters($items);

		$result = (object) array
		(
			"last_update" => $data->last_update,
			"content" => $items
		);

		echo(json_encode($result));
	}

	//Captures data from Stack Overflow API and persists it
	public function captureData($displayResult = true)
	{
		$url = "https://api.stackexchange.com/2.2/search?page=1&pagesize=99&order=desc&sort=activity&tagged=PHP&site=stackoverflow";

		$this->load->helper('get_url');

		//stackOverflow data uncompressed
		$data = get_url($url);
		$data = json_decode($data, true);
		$data['last_update'] = time();
		$data = json_encode($data);

		file_put_contents('resources/ds/soData.json', $data);

		if ($displayResult)
		{
			echo $data;
		}
	}

	//Retrieves the persisted data
	private function getData()
	{
		$file = "resources/ds/soData.json";

		$data = file_get_contents($file);
		$data = json_decode($data);
		
		$content = [];

		if(!isset($data->items)){
			self::captureData(false);

			$data = file_get_contents($file);
			$data = json_decode($data);
		}

		return $data;
	}

	private function setItemsStructure($data)
	{
		$content = [];

		foreach($data->items as $sourceItem)
		{
			$item = (object) array
			(
				"question_id" => $sourceItem->question_id,
				"title" => $sourceItem->title,
				"owner_name" => $sourceItem->owner->display_name,
				"score" => $sourceItem->score,
				"creation_date" => $sourceItem->creation_date,
				"link" => $sourceItem->link,
				"is_answered" => $sourceItem->is_answered
			);
			$content[] = $item;
		}

		return $content;
	}

	private function applyParameters($items)
	{
		if ($this->input->get('score') != "")
		{
			if (is_numeric($this->input->get('score')))
			{
				$items = self::filterScore($this->input->get('score'), $items);
			} //possibility to implement error message for invalid input
		}

		if ($this->input->get('sort') != "")
		{
			if (in_array($this->input->get('sort'), $this->SORT_OPTIONS))
			{
				$items = self::sort($this->input->get('sort'), $items);
			} //possibility to implement error message for invalid input
		}

		if ($this->input->get("page") != "" && $this->input->get("rpp") != "")
		{
			if (is_numeric($this->input->get("page")) && round($this->input->get("page")) > 0
				&& is_numeric($this->input->get("rpp")) && round($this->input->get("rpp")) > 0)
			{
				$items = self::paginate(round($this->input->get("rpp")), round($this->input->get("page")), $items);
			} //possibility to implement error message for invalid input
		}


		return $items;
	}

	private function filterScore($minScore, $items)
	{
		$result = [];

		foreach($items as $item)
		{
			if ($item->score >= $minScore)
			{
				$result[] = $item;
			}
		}

		return $result;
	}

	private function sort($sortBy, $items)
	{
		usort($items, function($a, $b) use ($sortBy) {

			if ($sortBy == "title" || $sortBy == "owner_name" || $sortBy == "link")
			{
				$lowerA = strtolower($a->$sortBy);
				$lowerB = strtolower($b->$sortBy);

				return $lowerA < $lowerB ? -1 : 1;
			}

		    return $a->$sortBy < $b->$sortBy ? -1 : 1;
		});

		return $items;
	}

	private function paginate($rpp, $page, $items)
	{
		if (count($items) > 0)
		{
			$items = array_slice($items, $rpp * ( $page - 1 ), $rpp);
		}

		return $items;
	}

}